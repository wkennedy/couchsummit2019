for YAML in $(find ./ -name *.yaml) ; do echo $YAML ; cfn-flip -i yaml -o json $YAML $(basename $YAML).template ; done

for JSON in $(find ./ -name "*.template") ; do echo "$JSON.svg" ; cat $JSON | ./cfviz | dot -Tsvg -o$JSON.svg ; echo "![$YAML]($JSON.svg)" >> Diagrams.md ; echo "## $YAML" >> Diagrams.md ; done

