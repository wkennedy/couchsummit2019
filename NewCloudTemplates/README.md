## Templates

These were mirrored from GitHub after announcement at Atlassian Summit 2019

## Setup

    pip install cfn-flip

    cfn-flip template.yaml template.json

Run makesvgs.sh to locate and convert the YAML format CF teamplates to JSON (NOT RECOMMENDED IN A PRODUCTION WORKFLOW) for processing with cloud-formation-viz


## References

- https://www.abhishek-tiwari.com/visualising-cloudformation-stack/

- https://github.com/benbc/cloud-formation-viz

## Diagrams
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./quickstart-crowd-master.template.yaml.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./BitbucketDataCenter.template.yaml.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./quickstart-jira-dc.template.yaml.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./JiraDataCenter.template.yaml.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./quickstart-confluence-master-with-vpc.template.yaml.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./CrowdDataCenter.template.yaml.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./quickstart-bitbucket-dc-with-vpc.template.yaml.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./CrowdDataCenterClone.template.yaml.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./quickstart-jira-dc-with-vpc.template.yaml.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./BitbucketServer.template.yaml.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./quickstart-for-atlassian-services.yaml.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./ConfluenceServer.template.yaml.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./ConfluenceDataCenterClone.template.yaml.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./quickstart-backmac-for-atlassian-services.yaml.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./quickstart-confluence-master.template.yaml.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./quickstart-forge-for-atlassian-services.yaml.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./ConfluenceServerClone.template.yaml.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./ConfluenceDataCenter.template.yaml.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./cloud-formation-viz/atlassian-aws-deployment/templates/elasticsearch/bitbucket-search-cluster.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./cloud-formation-viz/atlassian-aws-deployment/misc/unload_rds.properties.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./cloud-formation-viz/quickstart-jira-dc-with-vpc.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./JiraDataCenterClone.template.yaml.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
![./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml](./quickstart-bitbucket-dc.template.yaml.template.svg)
## ./quickstart-atlassian-jira/templates/quickstart-jira-dc.template.yaml
